" A minimal vimrc for new vim users to start with.
"
" Referenced here: http://vimuniversity.com/samples/your-first-vimrc-should-be-nearly-empty
"
" Original Author:	     Bram Moolenaar <Bram@vim.org>
" Made more minimal by:  Ben Orenstein
" Modified by :          Ben McCormick
" Last change:	         2014 June 8
"
" To use it, copy it to
"  for Unix based systems (including OSX and Linux):  ~/.vimrc
"  for Windows :  $VIM\_vimrc
"
"  If you don't understand a setting in here, just type ':h setting'.

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" No fucking beeps
set noerrorbells

set showcmd
set showmode

set noswapfile 
set nobackup
set nowritebackup
set autowrite 
set autoread

set ruler
set incsearch		" Show the search matches on time
set hlsearch		" Highlighting the search matches
set ignorecase		" Case insensitive search pattern
set smartcase		" Case insensitive search except for upper case search

" Set length of a line
set textwidth=79

" Set noruler for appropriate statusline
set noruler

" Proper encoding
set encoding=utf-8

" Set runtime path
" set rtp+=~/.vim/bundle/Vundle.vim
" call vundle#begin()

" Plugin 'VundleVim/Vundle.vim'
" Plugin 'tpope/vim-fugitive'

" call vundle#end()
" Switch syntax highlighting on
syntax on

" Enable file type detection and do language-dependent indenting.
filetype plugin indent on

" Show line numbers
set number

" Allow hidden buffers, don't limit to 1 file per window/split
set hidden

" Set history 
set history=100

" Refresh .vimrc configuration after changes
map <leader>rr :source ~/.vimrc<CR>

" Initial statusline
set statusline=%t
set laststatus=2

" Add vim-fugitive status to statusbar
" set statusline+=%{fugitive#statusline()}

set statusline +=%1*\ %n\ %*	"buffer number
set statusline +=%5*%{&ff}%*	"file format
set statusline +=%3*%y%*	"file type
set statusline +=%4*\ %<%F%*	"full path
set statusline +=%1*%=%5l%*	"current line
set statusline +=%2*/%L%*	"total lines
