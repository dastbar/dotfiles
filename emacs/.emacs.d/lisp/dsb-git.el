;;; All the magit-stuff with forge. -*- lexical-binding:t -*-
;;

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
	 ("C-c g" . magit-file-dispatch)))


(use-package forge
  :ensure t
  :after magit)


;; Getting ghub of the ground
;; (ghub-request "GET" "/user" nil
;; 	      :forge 'gitlab
;; 	      :host "gitlab.com/api/v4"
;; 	      :username "dastbar"
;; 	      :auth 'forge)

(provide 'dsb-git)
