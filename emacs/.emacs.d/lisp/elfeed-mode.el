;;; Elfeed configuration -*- lexical-binding:t -*-
;;;
;;
;;

(use-package elfeed
  :ensure t
  :init
  (bind-key "C-x w" 'elfeed)

  :config
  (setq-default elfeed-search-filter "@2-week-ago +unread")
  (setq elfeed-feeds
      '(("http://phoronix.com/rss.php" linux tech news phoronix)
	("https://www.archlinux.org/feeds/packages/any/" linux arch dev pkg)
	("https://www.archlinux.org/feeds/packages/x86_64/" linux arch dev pkg)
	("http://undeadly.org/cgi?action=rss" tech news bsd)
	("https://undeadly.org/errata/errata.rss" tech news bsd)
	("https://undeadly.org/errata/errata-1.rss" tech news bsd)
	("https://lwn.net/headlines/rss" linux tech news)
	("http://feeds.feedburner.com/d0od" tech news linux omgubuntu)
	("https://www.heise.de/newsticker/heise-top-atom.xml" tech news heise)
	("https://www.computerbase.de/rss/news.xml" tech news tests cbase)
	("https://rss.golem.de/rss.php?feed=RSS2.0" tech news tests golem)
	("http://rss.slashdot.org/Slashdot/slashdotMain" tech news slashdot)
	("https://www.theregister.co.uk/headlines.atom" tech news theregister elreg)
	("http://sachachua.com/blog/feed/" news tech emacs)
	("https://hnrss.org/frontpage" news hackernews)
	("https://blog.tecosaur.com/tmio/rss.xml" news emacs)
	("https://www.gamingonlinux.com/article_rss.php" gaming linux)
	("https://www.holarse-linuxgaming.de/rss_all.xml" gaming linux)
	("http://www.gamestar.de/news/rss/news.rss" gaming)
	("http://feeds.feedburner.com/tumblr/lgc" gaming linux)
	("https://linuxgameconsortium.com/feed" gaming linux)
	("http://nofilmschool.com/rss.xml" movies filmmaking screenwriting)
	("http://www.spiegel.de/politik/index.rss" nachrichten spiegel)
	("http://www.spiegel.de/schlagzeilen/tops/index.rss" nachrichten spiegel)
	("http://www.spiegel.de/schlagzeilen/eilmeldungen/index.rss" nachrichten spiegel eilmeldungen)
	("http://www.spiegel.de/wirtschaft/index.rss" nachrichten spiegel)
	("http://www.spiegel.de/kultur/index.rss" nachrichten spiegel)
	("http://www.spiegel.de/netzwelt/index.rss" nachrichten spiegel)
	("http://www.spiegel.de/gesundheit/index.rss" nachrichten spiegel)
	("http://newsfeed.zeit.de/index" nachrichten zeit)
	("http://www.faz.net/rss/aktuell/" nachrichten faz)
	("http://www.donaukurier.de/storage/rss/rss/kurzmeldungen.xml" nachrichten dk in)
	("http://www.donaukurier.de/storage/rss/rss/polizeimeldungen.xml" nachrichten dk in)
	("https://www.merkur.de/welt/rssfeed.rdf" nachrichten muc merkur)
	("https://www.theguardian.com/uk/rss" nachrichten news english)
	("http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml" nachrichten news english)
	("https://www.welt.de/feeds/latest.rss" nachrichten welt)
	("https://www.reddit.com/r/unixporn/.rss" linux unixporn desktop design)
	("https://www.reddit.com/r/archlinux/.rss" linux arch reddit)
	("https://www.reddit.com/r/Python/.rss" python coding programming reddit)
	("https://www.reddit.com/r/emacs/.rss" linux tech emacs)
	("https://juergenfritz.com/feed/" news blog)
	("https://cdn.jwz.org/blog/feed/" blog tech)
	("https://www.reddit.com/r/C_Programming/.rss" tech programming)
	("https://rss.sueddeutsche.de/rss/Topthemen" nachrichten sz)
	("https://rss.sueddeutsche.de/app/service/rss/alles/index.rss?output=rss" nachrichten sz)
	("https://rss.sueddeutsche.de/rss/Muenchen" nachrichten sz muc)
	("https://www.derstandard.at/rss" nachrichten austria)
	("https://www.theatlantic.com/feed/all/" news us)
	("https://news.itsfoss.com/latest/rss/" news linux tech itsfoss)
	("https://steamdeckhq.com/feed/" tech linux gaming steamdeck)
	("https://steamdeckhq.com/tips-and-guides/feed" tech linux gaming steamdeck)
	("https://steamdeckhq.com/feed/?post_type=game-reviews" tech linux gaming steamdeck))))


(provide 'elfeed-mode)
