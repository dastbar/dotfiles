;;; Definition for my org-mode setup -*- lexical-binding:t -*-
;;

(use-package org
  :ensure t
  :mode (("\\.org\\'" . org-mode)
	 ("\\.org_archive\\'" . org-mode))
  :bind (("C-c l" . org-store-link)
	 ("C-c C-l" . org-insert-link)
	 ("C-c c" . org-capture)
	 ("<f3>" . org-capture)
	 ("<f12>" . org-agenda)
	 ("C-c C-c" . org-set-tags-command)
	 ("<f9>" . calendar))
  :config
  (progn
    (setq org-directory "~/org")
    (setq org-agenda-files '("~/org/inbox.org"
			     "~/org/tickler.org"
			     "~/org/agenda.org"
			     "~/org/gtd.org"
			     "~/org/ideen.org"))
    (setq org-default-notes-file "~/org/inbox.org")
    (setq org-agenda-format-date
	  "%Y-%m-%d ----------------------------------------------------------")

    (setq org-capture-templates '(("t" "ToDo [Inbox]" entry
				   (file+headline "~/org/inbox.org" "Tasks")
				   "* TODO %i%?")
				  ("b" "Brainstorm / Ideen" entry
				   (file+headline "~/org/ideen.org" "Brainstorm/Ideen")
				   "* %i%? \n %U")
				  ("T" "Tickler" entry
				   (file+headline "~/org/tickler.org" "Tickler")
				   "* %i%? \n %U")))
    (setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "DEPEND(e)" "|" "DONE(d)" "CANCELLED(c)")))
    (setq org-export-coding-system 'utf-8)
    (setq org-refile-allow-creating-parent-nodes 'confirm)
    (setq org-log-done 'note)
    (setq org-archive-location (concat org-directory "/archiv/archived.org::"))
    (setq org-refile-targets
	  '(("~/org/gtd.org" :maxlevel . 3)
	    ("~/org/bucket.org" :maxlevel . 2)
	    ("~/org/tickler.org" :maxlevel . 2)))))

;;; Get the Encryption of the ground
;;  This is a built-in Emacs library

(require 'epa-file)
(epa-file-enable)
(setq epa-file-select-keys nil)


(provide 'dsb-org)

;;; dsb-org.el ends here

