;;; Common modes for different filetypes and languages -*- lexical-binding:t -*-
;;
;;

(use-package json-mode
  :ensure t
  :requires json-reformat
  :mode (("\\.json\\'" . json-mode)
	 ("\\.suites\\'" . json-mode)
	 ("\\.repos\\'" . json-mode)))

(use-package meson-mode
  :ensure t
  :mode "\\meson\\.build\\'")

(use-package cython-mode
  :ensure t
  :mode ("\\.pyx\\'" . cython-mode))

(use-package cmake-mode
  :ensure t
  :mode (("CMakeLists\\.txt'" . cmake-mode)
	 ("\\.cmake\\'" . cmake-mode)))

(use-package markdown-mode
  :ensure t
  :mode ("\\.md\\'" . markdown-mode))

(use-package systemd
  :ensure t)

(use-package fountain-mode
  :ensure t
  :mode ("\\.fountain\\'" . fountain-mode))

(use-package pkgbuild-mode
  :ensure t)

(use-package dracula-theme
  :ensure t
  :config
  (load-theme 'dracula t))

(use-package rust-mode
  :ensure t)

(use-package go-mode
  :ensure t)

(use-package adoc-mode
  :ensure t
  :mode ("\\.asciidoc\\'" . adoc-mode))

;; (use-package elpy
;;   :ensure t
;;   :config
;;   (elpy-enable))

(use-package yaml-mode
  :ensure t
  :mode ("\\.yaml\\'" . yaml-mode))

(use-package jinja2-mode
  :ensure t
  :mode ("\\.skel\\'" . jinja2-mode))

(use-package dockerfile-mode
  :ensure t
  :mode ("Dockerfile" . dockerfile-mode))

;; Unresolved dependency for magit-forge, therefore we have it
;; here explicitly
(use-package sqlite3
  :ensure t)

;; TODO: Rework this mode-definition in the long run.
;; It depends on the package "mermaid-cli" to get the full potential
(use-package mermaid-mode
  :ensure t
  :mode ("\\.mermaid\\'" . mermaid-mode))

(use-package terraform-mode
  :ensure t
  :mode ("\\.tf\\'" . terraform-mode))


(provide 'common-modes)

;; common-mode.el ends here
