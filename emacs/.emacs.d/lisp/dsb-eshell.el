;;; Emacs-Shell configuration -*- lexical-binding:t -*-
;;
;;

(require 'eshell)
(require 'esh-mode)

;; Starting shortcut
(global-set-key (kbd "C-c C-e") 'eshell)

;; Alias commands
(defalias 'nopen 'find-file)

(defun nesh ()
  "Create another eshell"
  (interactive)
  (eshell 'N))


;; General shell-settings (not eshell explicit)

(setq shell-file-name "bash")
(setq explicit-shell-file-name "/bin/bash")


(provide 'dsb-eshell)

;;; dsb-eshell.el ends here

