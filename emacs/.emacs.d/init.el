;;; init.el --- DSB/dastbar Emacs configuration -*- lexical-binding: t; -*-
;; 
;; Copyright (C) 2017-2024 Daniel Bartlau
;; 
;; This file is NOT part of GNU Emacs.
;; 
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; gc threshold - by default 800 kilobyte
;; -> 80 Megabyte
(setq gc-cons-threshold (* 80 1000 1000))

;; Package repositories
(require 'package)
	(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
	(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
(setq package-enable-at-startup nil)
(package-initialize)

;; Bootstrap our use-package environment (pre Emacs 29)
(if (version< emacs-version "29.1")
    (unless (package-installed-p 'use-package)
      (package-refresh-contents)
        (package-install 'use-package)))

(eval-when-compile (require 'use-package))
(setq use-package-always-ensure t)

(setq lisp-lib-dir
      (expand-file-name "lisp" user-emacs-directory))

(add-to-list 'load-path lisp-lib-dir)

;; Set path for the emacs-sourcecode
(if (file-symlink-p "/usr/src/emacs")
    (setq source-directory "/usr/src/emacs"))

;; Set fonts accordingly
(defun dsb-checklhm ()
  "Check for a file named '.lhm' in $HOME to 
set a appropriate font for our machine and make some other changes
regarding frames."
  (if (not (file-exists-p "~/.lhm"))
      (setq default-frame-alist '((font . "Fira Mono-11")))
    (setq default-frame-alist '((font . "Noto Mono-9"))))
  (add-to-list 'default-frame-alist '(fullscreen . maximized)))
      
;; Font encoding
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8-unix)
(dsb-checklhm) ;; Setting the correct fontsize

;; Visuals and behaviour
(global-display-line-numbers-mode t)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; (setq split-width-threshold 0)
;; (setq split-height-threshold nil)

;; Register options
;; Set newline when you append text to an existing register
(setq register-separator ?+)
(set-register register-separator "\n\n")

(setq-default
  inhibit-splash-screen t
  inhibit-startup-message t)

(setq backup-inhibit t) ;; No swapfiles
(setq auto-save-default nil) ;; No autosave
(setq make-backup-files nil)
(setq create-lockfiles nil)

;; Shutting down the fucking bell
(setq ring-bell-function #'ignore)

;;; Hooks
;; Set the hook for word-wrap
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)

;; Bookmark settings
(setq bookmark-default-file "~/.emacs.d/bookmarks")

;;; General options

(setq common-user "dastbar")
(setq full-name "Daniel Bartlau")
(setq full-name-user "daniel.bartlau")

(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
(display-time)

;; Calendar options
(setq calendar-week-start-day 1)

(setq auth-sources '("~/.authinfo"))

;; Setting the C-Language (default) style.
(setq c-default-style "linux" 
      c-basic-offset 4)

;; Setting line-wrap and newline at EOF
(setq-default fill-column 80)
(setq-default require-final-newline t)

(setq column-number-mode t)

;; Make a short answer
(defalias 'yes-or-no-p 'y-or-n-p)
(setq confirm-kill-emacs 'y-or-n-p)

(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file)

;;; Bind keys (Shortcuts)
;; Bookmarks
(global-set-key (kbd "<f6>") 'bookmark-set) ;; Writes bookmarks, no matter what.
(global-set-key (kbd "<f7>") 'bookmark-set-no-overwrite) ;; Dont allow overwrite bookmarks
(global-set-key (kbd "<f8>") 'bookmark-jump)

;; Load own lisp-packages / functions
(require 'common-modes)
(require 'elfeed-mode)
(require 'dsb-org)
(require 'dsb-git)
(require 'dsb-eshell)

