# Aliases for several purposes

## General commands

alias ls="ls --color"
alias tmacs="emacsclient -c -nw"


## Pacman commands

alias pacup="sudo pacman -Syu"
alias pacfind="sudo pacman -Ss"
alias pacrm="sudo pacman -Rc"
alias pacclean="sudo pacman -Scc"
alias paccache="sudo pacman -Sc"
alias pacslocal="sudo pacman -Qi"
alias paclook="sudo pacman -Si"
alias pacfetch="sudo pacman -Suw"
alias pacsync="sudo pacman -Sy"
alias pacsyncf="sudo pacman -Syy"

# Temporarily 

alias ard="mpv https://mcdn.daserste.de/daserste/de/master.m3u8"
alias zdf="mpv https://www.zdf.de/sender/zdf/zdf-live-beitrag-100.html"
alias 3sat="mpv https://zdf-hls-18.akamaized.net/hls/live/2016501/dach/high/master.m3u8"
alias br3="mpv https://mcdn.br.de/br/fs/bfs_sued/hls/de/master.m3u8"
alias arte="mpv https://artesimulcast.akamaized.net/hls/live/2030993/artelive_de/index.m3u8"
