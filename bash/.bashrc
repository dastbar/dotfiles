#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Deactivate bash-suspend which messes with some keybindings
stty -ixon

# The old prompt
#PS1='[\u@\h \W]\$ '
export PS1="\[\033[38;5;11m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

PATH=$HOME/.local/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl

# Options
export EDITOR=vim
export VISUAL=less
export CHROOT=$HOME/chroot
HISTSIZE=10000
HISTCONTROL=erasedups

# File include
source ~/.bash_aliases
